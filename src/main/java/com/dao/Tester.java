package com.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Tester {
public static void main(String[] args) {
	StudentDao dao=new StudentDaoImp();
List<Student>studentList=dao.getAllStudent();
studentList.forEach(s->System.out.println(s));
Map<Integer,String> studentsMap = new HashMap<Integer,String>();

studentsMap.put(90, "prathyu");
studentsMap.put(80, "vinay");
studentsMap.put(70, "shirisha");
studentsMap.put(60, "vandana");
studentsMap.put(50, "yash");
Set<Entry<Integer,String>>entryset=studentsMap.entrySet();
System.out.println("................result sorted as per marks (key).........");
entryset.stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
System.out.println("................result sorted as per name(value).........");
entryset.stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);



}


}

